package com.fallenbug.keysnooper.ui;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.fallenbug.keysnooper.R;
import com.fallenbug.keysnooper.common.Config;

import java.io.File;
import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private static final String LOG_TAG = "MainActivity";
    private static final int REQUEST_RECORD_AUDIO_PERMISSION = 200;
    private static final int REQUEST_WRITE_EXTERNAL_PERMISSION = 201;
    private static final int REQUEST_MULTIPLE_PERMISSION = 202;

    // Requesting permission to RECORD_AUDIO
    private boolean permissionsAccepted = false;
    private String [] permissions = {Manifest.permission.RECORD_AUDIO, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_EXTERNAL_STORAGE};

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case REQUEST_RECORD_AUDIO_PERMISSION:
                permissionsAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
            case REQUEST_WRITE_EXTERNAL_PERMISSION:
                permissionsAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                break;
            case REQUEST_MULTIPLE_PERMISSION:
                permissionsAccepted  = grantResults[0] == PackageManager.PERMISSION_GRANTED;
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActivityCompat.requestPermissions(this, permissions, REQUEST_MULTIPLE_PERMISSION);
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.main_fragment_holder ,new RecordFragment())
                .commit();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.main_fragment_holder ,new SettingFragment()).commit();
            return true;
        } else if(id == R.id.action_record){
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.main_fragment_holder ,new RecordFragment()).commit();
            return true;
        } else if (id == R.id.action_share){

            File file1 = new File(Config.BASE_PATH+File.separator+Config.FOLDER+File.separator+Config.AudioFile);
            File file2 = new File(Config.BASE_PATH+File.separator+Config.FOLDER+File.separator+Config.GyroFile);
            File file3 = new File(Config.BASE_PATH+File.separator+Config.FOLDER+File.separator+Config.AccFile);
            if (file1.exists() || file2.exists() || file3.exists()){
                Log.e("Main","not file exists");
                return false;
            }
            Intent i = new Intent(Intent.ACTION_SEND_MULTIPLE);
            i.putExtra(Intent.EXTRA_SUBJECT, "Snooper Recorded data");
            i.putExtra(Intent.EXTRA_EMAIL, new String[]{Config.TO_EMAIL});
            SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(this);
            ArrayList<Uri> uris = new ArrayList<>();
            if (mSettings.getBoolean("audio_enable_preference", false)){
                uris.add(Uri.fromFile(file1));
            }
            if (mSettings.getBoolean("gyro_enable_preference", false)){
                uris.add(Uri.fromFile(file2));
            }
            if (mSettings.getBoolean("acc_enable_preference", false)){
                uris.add(Uri.fromFile(file3));
            }
            i.putParcelableArrayListExtra(Intent.EXTRA_STREAM, uris);
            i.setType("*/*");

            startActivity(i);
        }

        return super.onOptionsItemSelected(item);
    }
}
