package com.fallenbug.keysnooper.ui;


import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioFormat;
import android.media.AudioRecord;
import android.media.MediaRecorder;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fallenbug.keysnooper.R;
import com.fallenbug.keysnooper.common.Config;
import com.fallenbug.keysnooper.utils.FFT;
import com.fallenbug.keysnooper.utils.Utils;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;

import static android.content.Context.SENSOR_SERVICE;
import static com.fallenbug.keysnooper.common.Config.ACC_THRESHOLD;
import static java.lang.Math.abs;


public class RecordFragment extends Fragment implements SensorEventListener{

    final String LOG_TAG = this.getClass().getName();

    Button btn_start_record;
    Button btn_stop_record;
    TextView tv_recorder_status;
    EditText et_sequence;

    final int AUDIO_SAMPLE_RATE = 44100;
    boolean audioShouldContinue;
    private SensorManager sensorManager;

    BufferedWriter gyro_file_writer, acc_file_writer;
    FileOutputStream audio_file_writer = null;
    boolean gyro_enabled, acc_enabled, audio_enabled;
    GraphView graph, graph1;
    LineGraphSeries<DataPoint> mSeries, mSeries1, mSeries2, audioSeries;
    double graphLastXValue = 0d, graph2LastXValue = 0d, graphLastAudValue = 0d;

    Boolean keyRecord=false, GyroStart=false, AudioStart=false;
    ArrayList<Double> bufferGyro, bufferAcc, bufferAudio;
    String trainingStr, currentKey;
    int signalLengthCount =0, currentKeyIndex;
    long charNum =0;
    double gyroAvg=0, accRunningAvg=0, avgAccFFt=0;

    FFT fft;
    int N=32;
    double[] re = new double[N];
    double[] im = new double[N];

    public RecordFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_record, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        btn_start_record = (Button) view.findViewById(R.id.btn_start_record);
        btn_stop_record = (Button) view.findViewById(R.id.btn_stop_record);
        tv_recorder_status = (TextView) view.findViewById(R.id.tv_recorder_status);
        et_sequence = (EditText) view.findViewById(R.id.et_sequence);

        sensorManager = (SensorManager) getActivity().getSystemService(SENSOR_SERVICE);

        graph = (GraphView)view.findViewById(R.id.graph);
        graph1 = (GraphView)view.findViewById(R.id.graph1);
        mSeries = new LineGraphSeries<>();
        mSeries1 = new LineGraphSeries<>();
        mSeries2 = new LineGraphSeries<>();
        audioSeries = new LineGraphSeries<>();
        graph.addSeries(mSeries);
        graph1.addSeries(mSeries1);
        graph1.addSeries(mSeries2);
        graph.getViewport().setXAxisBoundsManual(true);
        graph.getViewport().setMinX(0);
        graph.getViewport().setMaxX(400);
        graph.getViewport().setYAxisBoundsManual(true);
        graph.getViewport().setMinY(-20d);
        graph.getViewport().setMaxY(20d);

        graph1.getViewport().setXAxisBoundsManual(true);
        graph1.getViewport().setMinX(0);
        graph1.getViewport().setMaxX(400);
        graph1.getViewport().setYAxisBoundsManual(true);
//        graph1.getViewport().setMinY(-1d);
//        graph1.getViewport().setMaxY(1d);
        graph1.getViewport().setMinY(8.5d);
        graph1.getViewport().setMaxY(11d);
//        graph1.getViewport().setScalableY(true);

        fft = new FFT(N);
        for (int i=0;i<N;i++){
            re[i] = im[i] = 0;
        }

        trainingStr = "";
        bufferGyro = new ArrayList<>();
        bufferAcc = new ArrayList<>();
        bufferAudio = new ArrayList<>();

        SharedPreferences mSettings = PreferenceManager.getDefaultSharedPreferences(getActivity());
        audio_enabled = mSettings.getBoolean("audio_enable_preference", false);
        gyro_enabled = mSettings.getBoolean("gyro_enable_preference", false);
        acc_enabled = mSettings.getBoolean("acc_enable_preference", false);


        btn_start_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (audio_enabled) {startRecording();audioShouldContinue=true;}
                startSensorsRecording();
                tv_recorder_status.setText("Recording Started");
                trainingStr = et_sequence.getText().toString();
            }
        });
        btn_stop_record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (audio_enabled){stopRecording();audioShouldContinue=false;}
                stopSensorRecording();
                tv_recorder_status.setText("Recording Stopped");
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
//        if (recorder != null) {
//            recorder.release();
//            recorder = null;
//        }
        stopSensorRecording();
    }

    void startRecording() {

        new Thread(new Runnable() {
            @Override
            public void run() {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_AUDIO);
                // buffer size in bytes
                int bufferSize = AudioRecord.getMinBufferSize(AUDIO_SAMPLE_RATE,
                        AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT);

                if (bufferSize == AudioRecord.ERROR || bufferSize == AudioRecord.ERROR_BAD_VALUE) {
                    bufferSize = AUDIO_SAMPLE_RATE * 2;
                }

                short[] audioBuffer = new short[bufferSize / 2];

                AudioRecord record = new AudioRecord(MediaRecorder.AudioSource.DEFAULT,
                        AUDIO_SAMPLE_RATE, AudioFormat.CHANNEL_IN_MONO,
                        AudioFormat.ENCODING_PCM_16BIT, bufferSize);

                if (record.getState() != AudioRecord.STATE_INITIALIZED) {
                    Log.e(LOG_TAG, "Audio Record can't initialize!");
                    return;
                }
                record.startRecording();

                Log.v(LOG_TAG, "Start recording");

                while (audioShouldContinue) {
                    int numberOfShort = record.read(audioBuffer, 0, audioBuffer.length);
                    // Do something with the audioBuffer

                    if (AudioStart) {
                        AudioStart = false;
                        try {
                            audio_file_writer = new FileOutputStream(
                                    Utils.get_file_path(charNum, "audio", currentKey));
                        } catch (IOException e) {
                            Log.d(getClass().toString(), "cant create audio file");
                        }
                    }

                    if (keyRecord) {
                        try {
                            byte bData[] = short2byte(audioBuffer);
                            audio_file_writer.write(bData, 0, audioBuffer.length * 2);
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
                record.stop();
                record.release();
            }
        }).start();
    }
    void stopRecording() {
//        recorder.stop();
//        recorder.release();
//        recorder = null;
    }

    private byte[] short2byte(short[] sData) {
        int shortArrsize = sData.length;
        byte[] bytes = new byte[shortArrsize * 2];

        for (int i = 0; i < shortArrsize; i++) {
            bytes[i * 2] = (byte) (sData[i] & 0x00FF);
            bytes[(i * 2) + 1] = (byte) (sData[i] >> 8);
            sData[i] = 0;
        }
        return bytes;

    }

    void startSensorsRecording(){

        if(gyro_enabled) {
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE), SensorManager.SENSOR_DELAY_FASTEST);
        }

        if(acc_enabled) {
            sensorManager.registerListener(this, sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER), SensorManager.SENSOR_DELAY_FASTEST);
        }
    }

    void stopSensorRecording(){
        sensorManager.unregisterListener(this);
//        try {
//            gyro_file_writer.close();
//            acc_file_writer.close();
//        } catch (IOException e) {
//            Log.e(getClass().toString(), "Error in closing sensors file");
//        }
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GYROSCOPE && gyro_enabled){
            float x = sensorEvent.values[0];
            float y = sensorEvent.values[1];

            float final_value = x + y;
//            graphLastXValue += 1d;
//            mSeries2.appendData(new DataPoint(graphLastXValue, final_value),true, 400);
//            try {
//                gyro_file_writer.append(Float.toString(x) + " " + Float.toString(y) + " ");
//                gyro_file_writer.newLine();
//            } catch (IOException e){
//                Log.e(getClass().toString(), "cant write to sensor file");
//            }

            bufferGyro.add(Double.valueOf(final_value));
            if (bufferGyro.size()>= Config.BUFFER_SIZE){
                bufferGyro.remove(0);
            }

            if (GyroStart){
                GyroStart=false;
                try {
                    gyro_file_writer= new BufferedWriter(new FileWriter(new File(
                            Utils.get_file_path(charNum, "gyro", currentKey))));
                } catch (IOException e){
                    Log.d(getClass().toString(), "cant create sensor file");
                }

                for (int i=0;i<bufferGyro.size();i++){
                    try {
                        gyro_file_writer.append(bufferGyro.get(i)+ " ");
                        gyro_file_writer.newLine();
                    } catch (IOException e){
                        Log.e(getClass().toString(), "cant write to sensor file");
                    }
                }
            }

            if (keyRecord){
                try {
                    gyro_file_writer.append(Float.toString(final_value)+ " ");
                    gyro_file_writer.newLine();
                } catch (IOException e){
                    Log.e(getClass().toString(), "cant write to sensor file");
                }
            }
        }

        if (sensorEvent.sensor.getType() == Sensor.TYPE_ACCELEROMETER && acc_enabled){
            float z = sensorEvent.values[2];
//            accRunningAvg = 0.7*accRunningAvg + 0.3*z;
//            z = (float) accRunningAvg;
//            z = (float) Math.cos(2*Math.PI*(int)(graph2LastXValue % N) / N);
//            z = (int)(graph2LastXValue % N);
//            if ((int)(graph2LastXValue % N) < 10) z = 1;
//            else z=0;

            if ((int)(graph2LastXValue % N) == 0){
//                new processSensorAsync().execute(re);
//                DataPoint dps[] = new DataPoint[N];
//
//                try {
//                    gyro_file_writer.newLine();
//                    gyro_file_writer.newLine();
//                } catch (IOException e){
//                    Log.e(getClass().toString(), "cant write to sensor file");
//                }
//
//                for (int i=0;i<N;i++){
//                    dps[i] = new DataPoint(i, re[i]);
//                    try {
//                        gyro_file_writer.append(String.valueOf(re[i]));
//                        gyro_file_writer.newLine();
//                    } catch (IOException e){
//                        Log.e(getClass().toString(), "cant write to sensor file");
//                    }
//                }
//                mSeries1.resetData(dps);
//                DataPoint[] fft_points = do_fft(re);
                avgAccFFt = do_avg_fft(re);
//                Log.d(LOG_TAG, String.valueOf(avgAccFFt));
//                mSeries.resetData(fft_points);
            }

            re[(int)(graph2LastXValue % N)] = z;
            graph2LastXValue += 1d;
            mSeries1.appendData(new DataPoint(graph2LastXValue, z),true, 400);

            bufferAcc.add(Double.valueOf(z));
            if (bufferAcc.size()>= Config.BUFFER_SIZE){
                bufferAcc.remove(0);
            }

            if (keyRecord){
                if (signalLengthCount < Config.SIGNAL_LENGTH_ACC){
                    try {
                        acc_file_writer.append(Float.toString(z)+ " ");
                        acc_file_writer.newLine();
                    } catch (IOException e){
                        Log.e(getClass().toString(), "cant write to sensor file");
                    }
                    signalLengthCount++;
                } else {
                    try {
                        if(acc_file_writer != null) acc_file_writer.close();
                        if(gyro_file_writer != null) gyro_file_writer.close();
                        if(audio_file_writer != null) audio_file_writer.close();
                    } catch (IOException | NullPointerException  e){
                        Log.e(getClass().toString(), "cant close files");
                    }
                    keyRecord = false;
                    signalLengthCount =0;
                }
            }

            if (!keyRecord && avgAccFFt > ACC_THRESHOLD){
                keyRecord =  GyroStart= AudioStart = true;
                currentKeyIndex =  (currentKeyIndex + 1)%trainingStr.length();
                currentKey = trainingStr.substring(currentKeyIndex, currentKeyIndex+1);
                if(currentKey.equals(" "))currentKey="sp";
                Log.d(LOG_TAG, "New file created");

                try {
                    acc_file_writer = new BufferedWriter(new FileWriter(new File(
                            Utils.get_file_path(++charNum, "acc", currentKey))));
                } catch (IOException e){
                    Log.d(getClass().toString(), "cant create sensor file");
                }

                for (int i=0;i<bufferAcc.size();i++){
                    try {
                        acc_file_writer.append(bufferAcc.get(i)+ " ");
                        acc_file_writer.newLine();
                    } catch (IOException e){
                        Log.e(getClass().toString(), "cant write to sensor file");
                    }
                }
                signalLengthCount += bufferAcc.size();
            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {}

    DataPoint[] do_fft(double[] re_in){
        DataPoint[] re_out = new DataPoint[N];

        fft.fft(re_in, im);
        for (int i=N-1;i>=N/2;i--){
            re_out[i-(N/2)] = new DataPoint(i-N/2, re_in[i]);
            im[i] = 0;
        }

        for (int i=0;i<N/2;i++){
            re_out[i+N/2] = new DataPoint(i+N/2, re_in[i]);
            im[i] = 0;
        }
        re_out[N/2] = new DataPoint(N/2,0);
        return re_out;

    }

    double do_avg_fft(double[] re_in){
        fft.fft(re_in, im);
        re_in[N/2] = 0;
        double avgFFT=0;
        for (int i=0;i<N;i++){
            avgFFT += abs(re_in[i]);
            im[i]=0;
        }
        return avgFFT/N;
    }
}
