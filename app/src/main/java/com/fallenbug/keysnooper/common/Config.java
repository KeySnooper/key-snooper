package com.fallenbug.keysnooper.common;

import android.os.Environment;

/**
 * Created by nithin on 20/8/17.
 */

public class Config {
    public static String BASE_PATH = Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String FOLDER = "KeySnooper";
    public static String AudioFile = "audio_snopper.pcm";
    public static String GyroFile = "gyro_snopper.txt";
    public static String AccFile = "acc_snopper.txt";

    public static String TO_EMAIL = "imnmfotmal@gmail.com";

    public static int ACC_THRESHOLD = 10;
    public static int BUFFER_SIZE = 50;
    public static int SIGNAL_LENGTH_ACC = 150;

}
