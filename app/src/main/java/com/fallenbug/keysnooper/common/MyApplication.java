package com.fallenbug.keysnooper.common;

import android.app.Application;

import com.facebook.stetho.Stetho;

/**
 * Created by nithin on 12/7/17.
 */

public class MyApplication extends Application {
    public void onCreate() {
        super.onCreate();
        Stetho.initializeWithDefaults(this);
    }
}
